# README #

> A dive into node.js. This is a simple node app to detail some web fundamentals. 
> This is project #1 for Lyons University week 3.

### What is this repository for? ###

> As a "background" task to keep the employees engaged and breakup the monotomy of just watching videos, we have a simple Nodejs project for the LyonsU employees to work on.
> We will do a deep-dive into node.js later, but we will start with a simple web-app that just scratches the surface of node, but confirms some web fundamentals.

##### Quick summary #####

* Version 1.2
* Implementation of 
	* MongoDb for backend database
	* ExpressJs for backend Server and routing
	* NodeJs for backend Server
	* Express-Handlebars for template rendering
		* Conjuction with Bootstrap for front-end page rendering

### How do I get set up? ###

* Summary of installation
	* To setup just download the repo
	* `cd "location" open app.js`
	* got to line => `mongoose.connect('mongodb://<userName>:<userPasword>@ds259117.mlab.com:59117/dev-testing-db')`
	* change <userName>:<userPasword> to your name and password
* Configuration
	* to start run
		* `node app.js'
		* or 
		* `nodemon app.js localhost 3001`
	* got to http://localhost:3001/api

### Who do I talk to? ###

* Repo owner or admin
* LyonsU Coordinator or admin