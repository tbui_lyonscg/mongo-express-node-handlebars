'use strict'

var path = require('path')
var exphbs = require('express-handlebars')
var express = require('express')
var mongoose = require('mongoose')
var bodyParser = require('body-parser')
var cookieParser = require('cookie-parser')

var app = express()
var router = require('./routers/router')

// view engine setup
app.set('views', path.join(__dirname, 'views'))
// In case you have any static files to be loaded
app.use(express.static(path.join(__dirname, 'public')))

app.engine('handlebars', exphbs({defaultLayout: 'main'}))
app.set('view engine', 'handlebars')

var port = process.env.PORT || 3001

mongoose.connect('mongodb://<userName>:<userPassword@ds259117.mlab.com:59117/dev-testing-db')
var db = mongoose.connection;
db.on('error', console.error.bind(console, 'connection error:'));

app.use(bodyParser.urlencoded({ extended: true }))
app.use(bodyParser.json())

// need cookieParser middleware before we can do anything with cookies
app.use(cookieParser())

// To prevent errors from Cross Origin Resource Sharing, we will set our headers to allow CORS with middleware like so:
app.use(function(req, res, next) {
	res.setHeader('Access-Control-Allow-Origin', '*')
	res.setHeader('Access-Control-Allow-Credentials', 'true')
	res.setHeader('Access-Control-Allow-Methods', 'GET,HEAD,OPTIONS,POST,PUT,DELETE')
	res.setHeader('Access-Control-Allow-Headers', 'Access-Control-Allow-Headers, Origin,Accept, X-Requested-With, Content-Type, Access-Control-Request-Method, Access-Control-Request-Headers')

	// and remove cacheing so we get the most recent comments
	res.setHeader('Cache-Control', 'no-cache')

	// set our own cookie
	// check if clilent sent cookie
	var cookie = req.cookies.cookieName
	if (cookie === undefined) {
		// no: set a new cookie
		var randomNumber = Math.random().toString()
		randomNumber = randomNumber.substring(2, randomNumber.length)
		res.cookie('cookieName',randomNumber, { maxAge: 900000, httpOnly: true })
	} else 
		console.log('cookie exists', cookie)

	next()
})

app.use('/api', router);
app.listen(port, function() {
console.log(`Server is running on localhost:${port}`)
})
