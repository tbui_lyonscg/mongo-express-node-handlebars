var express = require('express')
var Comment = require('../model/comments')
//var StyleSheets = require('../public/stylesheets')

var router = express.Router()
var user = []

// GET Home page
router.get('/', function(req, res, next) {
	console.log('name: ' + req.query['name'])
	res.render('home', { userName: req.query['name'] })
})

// GET headers
router.route('/headers')
.get(function(req, res) {
	res.render('headers', { headings: JSON.stringify(req.headers, null, '\t') })
})

// Get database info
router.route('/comments')
.get(function(req, res) {
	Comment.find(function(err, comments) {
		if(err)
			res.send(err)
		res.render('dbInformation', { dbInformation: JSON.stringify(comments, null, 2) })
	})
})

router.route('/album')
.get(function(req, res, next) {
	res.render('album')
})

// router.get('/api', function(req, res) {
// 	consoler.log('name: ' + req.query['name'])
// 	//res.redirect('home', { userName: req.body.name})
// })

// Express PUT
// Adding a route to a specific comment based on the database ID
router.route('/comments/:comment_id')
// The put method gives us the chance to update our comment based on the ID passed to the route
.put(function(req, res) {
	Comment.findById(req.params.comment_id, function(err, comment) {
		if (err)
			res.send(err)
		// setting the new email, name and text to whatever was changed. If nothing was changed
    	// we will not alter the field.
    	(req.body.email) ? comment.email = req.body.email : null
		(req.body.name) ? comment.name = req.body.name : null
		(req.body.text) ? comment.text = req.body.text : null
		comment.save(function(err) {
			if (err)
				res.send(err)
			res.json({ message: 'Comment has been updated' })
		})
	})
})

// Express POST
// post new comment to the database
router.route('/comments')
.post(function(req, res) {
	var comment = new Comment()
	comment.email = req.body.email
	comment.name = req.body.name
	comment.text = req.body.text	

	comment.save(function(err) {
	if (err)
		res.send(err)
	res.json({ message: 'Comment successfully added!' })
	})
})

router.route('/')
.post(function(req, res) {
	var comment = new Comment()
	comment.email = req.body.email
	comment.name = req.body.name
	comment.text = req.body.text

	user = req.body.name
	console.log(user)

	comment.save(function(err) {
	if (err)
		res.send(err)
	//res.json({ message: 'Comment successfully added!' })
	})

	//user.push(req.body)
})

// Express DELETE
// delete method for removing a comment from our database
router.route('/comments')
.delete(function(req, res) {
	// selects the comment by its ID, then removes it.
	Comment.remove({ _id: req.params.comment_id }, function(err, comment) {
		if (err)
			res.send(err)
		res.json({ message: 'Comment has been deleted' })
	})
})

router.route('/')
.delete(function(req, res) {
	// selects the comment by its ID, then removes it.
	Comment.remove({ _id: req.params.comment_id }, function(err, comment) {
		if (err)
			res.send(err)
		//res.json({ message: 'Comment has been deleted' })
	})
})

module.exports = router